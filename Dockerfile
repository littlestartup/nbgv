FROM alpine:3.19.0@sha256:51b67269f354137895d43f3b3d810bfacd3945438e94dc5ac55fdac340352f48
RUN apk add --update bash wget curl
RUN apk add dotnet6-sdk
RUN dotnet tool install --tool-path /usr/local/bin nbgv
